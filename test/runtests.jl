using UnitfulME
using UnitfulME:
    lbm, tonne, ton_us, klbs,
    kWh, Btu_it, Btu_th, tTNT,
    inH2O, mmHg,
    hpm, hpe

using Unitful:
    lb, kg,
    hr,
    lbf,
    Pa, atm,
    MJ,
    W, kW

using Test

@testset "mass" begin
    @test 1lbm == 1lb
    @test 1tonne == 1000kg
    @test 1ton_us == 2000lbm
    @test 1ton_us == 2000lb
    @test 1tonne/klbs == 1kg/lbm
end

@testset "energy" begin
    @test 1kWh == 3.6MJ
    @test 1tTNT == 4184MJ
end

@testset "power" begin
    @test 1hpe == 746W
    @test 1hpm == 550u"ft*lbf/s"
    @test 1inH2O / 2.490889e2Pa ≈ 1 atol=0.001  #1 
    @test 1u"TR" == 12000Btu_it/hr
end

@testset "head|pressure" begin
    @test (1inH2O / 2.490889e2Pa) ≈ 1 atol=0.001  #1 
    @test 760mmHg / 1atm ≈ 1 atol=0.001   
end




#1: NIST SP-811: inch of water, conventional (inH2O) = 2.490_889E+02 pascal (Pa)