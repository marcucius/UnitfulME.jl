UnitfulME.jl
=============

Extends Unitful.jl with units commonly encountered in undergraduate Mechanical Engineering curricula.

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mdAshford.gitlab.io/UnitfulME.jl/dev)
<!-- [![Build Status](https://github.com/mdAshford/UnitfulME.jl/badges/main/pipeline.svg)](https://github.com/mdAshford/UnitfulME.jl/pipelines)
[![Coverage](https://github.com/mdAshford/UnitfulME.jl/badges/main/coverage.svg)](https://github.com/mdAshford/UnitfulME.jl/commits/main) -->
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)


What's inside
--------------- 


- **lbm** ⋮ pound-mass ⋮ nomenclature explicity indicates mass

- **klbs** ⋮ kilopound ⋮ common in the petrochemical industry

- **tonne** ⋮ metric ton ⋮ nomenclature explicity indicates metric ton

- **ton** ⋮ US short ton

- **kWh** ⋮ kilowatt-hours ⋮ for convenience

- **tTNT** ⋮ tonne TNT equivalent ⋮ for fun

- **calᴵᵀ** ⋮ international Table calorie ⋮ for building/converting units based on the IT calorie

- **calₜₕ** ⋮ thermochemical calorie ⋮ for building/converting units based on the thermochemical calorie
 
- **Btuᴵᵀ** ⋮ international table ⋮ for building/converting units based on the international table Btu

- **Btuₜₕ** ⋮ thermochemical British thermal unit ⋮ for building/converting units based on the thermochemical Btu

- **quad** ⋮ commonly used for nation-scale quantities of energy 

- **hpᴹ** ⋮ mechanical horsepower

- **hpᴱ** ⋮ electrical horsepower

- **TR** ⋮ ton of refrigeration

- **inH₂O**; **inWC** ⋮ inches water ⋮ manometer height

- **mmHg**, **inHg** ⋮ millimeters, inches mercury ⋮  manometer height




References
----------

1. Butcher, K. S., Crown, L. D., & Gentry, E. J. (2006). _The international system of units (SI)—Conversion factors for general use_ (NIST SP 1038; 0 ed., p. NIST SP 1038). National Institute of Standards and Technology. https://doi.org/10.6028/NIST.SP.1038

1. Newell, D. B., & Tiesinga, E. (2019). _The international system of units (SI): 2019 edition_ (NIST SP 330-2019). National Institute of Standards and Technology. https://doi.org/10.6028/NIST.SP.330-2019

1. Thompson, E. A., & Taylor, B. N. (2008). _Guide for the use of the International System of Units (SI)_ (NIST SP 811e2008; 0 ed., p. NIST SP 811e2008). National Institute of Standards and Technology. https://doi.org/10.6028/NIST.SP.811e2008

1. Williams, D. R. (2021, December 21). _Earth Fact Sheet._ NASA Goddard Space Flight Center. https://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html


