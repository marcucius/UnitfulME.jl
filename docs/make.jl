using UnitfulME
using Documenter

DocMeta.setdocmeta!(UnitfulME, :DocTestSetup, :(using UnitfulME); recursive=true)

makedocs(;
    modules=[UnitfulME],
    authors="mcs <marcus@psydat.com> and contributors",
    repo="https://github.com/mdAshford/UnitfulME.jl/blob/{commit}{path}#{line}",
    sitename="UnitfulME.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mdAshford.gitlab.io/UnitfulME.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
