__precompile__(true)


"""
"""
module UnitfulME

using Reexport
@reexport using Unitful

import Unitful: @unit,  lb,kg,  ft,  s,hr,  lbf,  Pa,  cal,J,  W
import UnitfulUS: ton_us


#═══  Symbol     Disp  Name                                Equivalence  SIpre  Notes  ═══*═
# mass
@unit lbm       "lbm"  PoundsMass                                  1lb  false  
@unit klbs       "k#"  KiloPoundsMass                           1000lb  false 
@unit tonne   "tonne"  MetricTonne                              1000kg  false 
# @unit ton_us  "tonᵘˢ"  USTon                                    2000lb  false

# energy
@unit kWh       "kWh"  KiloWattHour                           1000W*hr  false 
@unit tTNT     "tTNT"  TonTNTequivalent                         1e9cal  true   #dfn
@unit cal_it  "calᴵᵀ"  InternationalTableCalorie               4.1868J  false  #tt #dfn #1 #2
@unit cal_th  "calₜₕ"  ThermochemicalCalorie                    4.184J  false  #tt #dfn #1 #2
@unit Btu_it  "Btuᴵᵀ"  InternationalTableBritishThermalUnit  1055.055_852_62J  false  #tt #ext #2
@unit Btu_th  "Btuₜₕ"  ThermochemicalBritishThermalUnit      1054.350J  false  #tt
@unit quad     "quad"  QuadrillionBritishThermalUnit        1e15Btu_it  false  #tt #dfn


# power
@unit hpm       "hpᴹ"  HorsepowerMechanical                550ft*lbf/s  false  #tt #dfn
@unit hpe       "hpᴱ"  HorsepowerElectric                         746W  false  #tt #ext
@unit TR         "TR"  TonOfRefrigeration               12000Btu_it/hr  false  #tt #dfn

# pressure/head
@unit inH2O   "inH₂O"  InchesWater                           249.082Pa  false  #bcg
@unit inWC     "inWC"  InchesWaterColumn                     249.082Pa  false  #bcg
@unit mmHg     "mmHg"  MillimetersMercury                  133.322_4Pa  false  #bcg
@unit inHg     "inHg"  InchesMercury                        3_386.38Pa  false  #bcg



# Allow precompile, and register mol units with u_str macro.
const localunits = Unitful.basefactors
function __init__()
    merge!(Unitful.basefactors, localunits)
    Unitful.register(UnitfulME)
end


end # module


#=═══════════════════════════════════════════════════════════════════════════════════════*═

Notes
-----

#1: These are true calories, not Calorie = kcal

#2: SP-811: NIST Guide to the SI, Footnote 9: The Fifth International Conference on the Properties of Steam (London, July 1956) defined the International Table calorie as 4.1868 J. Therefore, the exact conversion factor for the International Table Btu is 1.055 055 852 62 kJ. Note that the notation for International Table used in this listing is subscript "IT." Similarily, the notation for thermochemical is subscript "th." Further, the thermochemical Btu, Btuth, is based on the thermochemical calorie, calth, where calth = 4.184 J exactly.


References
----------

#bcg: 
Butcher, K. S., Crown, L. D., & Gentry, E. J. (2006). The international system of units 
    (SI)—Conversion factors for general use (NIST SP 1038; 0 ed., p. NIST SP 1038). 
    National Institute of Standards and Technology. https://doi.org/10.6028/NIST.SP.1038

#nt: 
Newell, D. B., & Tiesinga, E. (2019). The international system of units (SI): 2019 edition 
    (NIST SP 330-2019). National Institute of Standards and Technology. 
    https://doi.org/10.6028/NIST.SP.330-2019

#tt: 
Thompson, E. A., & Taylor, B. N. (2008). Guide for the use of the International System 
    of Units (SI) (NIST SP 811e2008; 0 ed., p. NIST SP 811e2008). National Institute of 
    Standards and Technology. https://doi.org/10.6028/NIST.SP.811e2008

#w: 
Williams, D. R. (2021, December 21). Earth Fact Sheet. NASA Goddard Space Flight Center.
     https://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html



=#






